﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Homework_08
{
    class InfoSystem
    {/// <summary>
    ///лист отделов 
    /// </summary>
        private ObservableCollection<Department> DepartmentList = new ObservableCollection<Department>();
        /// <summary>
        /// получить все отделы
        /// </summary>
        public ObservableCollection<Department> GetDepartments
        {
            get { return DepartmentList; }
        }
        /// <summary>
        /// получить количество отделов
        /// </summary>
        public int DepartmentsCount
        {
            get { return DepartmentList.Count(); }
        }
        /// <summary>
        /// получить лист работников из отдела
        /// </summary>
        /// <param name="index"></param> индекс отдела
        /// <returns></returns> лист с работниками
        public ObservableCollection<Worker> GetWorkerList(int index)
        {
             return DepartmentList[index].GetWorkers;
        }
        /// <summary>
        /// добавить работника в отдел
        /// </summary>
        /// <param name="index"></param> индекс отдела
        /// <param name="worker"></param> клас работник
        public void AddWorkerToDep(int index, Worker worker)
        {
            worker.Department = DepartmentList[index].Name;
            //здесь пропишу в воркера название отдела, 
            //можно наверное и до вызова при создании класса воркера но вроде там нет доступа к самому листу а писать получение имени нет смысла
            DepartmentList[index].AddWorker(worker);
        }/// <summary>
        /// удалить работника из отдела
        /// </summary>
        /// <param name="index"></param> ид отдела
        /// <param name="delworker"></param> ид работника в отделе
        public void DelWorkerToDep(int index, int delworker)
        {
            DepartmentList[index].DelWorker(delworker);
        }
        /// <summary>
        ///сортировать работников 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="field2"></param>
        /// <param name="index"></param>
        public void sortRecord(int field, int field2, int index)
        {
            DepartmentList[index].sortRecord(field, field2);
        }
        /// <summary>
        /// выгрузить в XML
        /// </summary>
        /// <param name="filepath"></param>
        public void ExportDatatoXML(string filepath)
        {
            filepath += "\\ExportDataXML.xml";
            // Создаем сериализатор на основе указанного типа 
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ObservableCollection<Department>));

            // Создаем поток для сохранения данных
            Stream fStream = new FileStream(filepath, FileMode.Create, FileAccess.Write);

            // Запускаем процесс сериализации
            xmlSerializer.Serialize(fStream, DepartmentList);

            // Закрываем поток
            fStream.Close();
        }
        /// <summary>
        /// Загрузить из XML
        /// </summary>
        /// <param name="Path"></param>
        public void ImportDataXML(string Path)
        {
            ObservableCollection<Department> dlist = new ObservableCollection<Department>();
            // Создаем сериализатор на основе указанного типа 
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ObservableCollection<Department>));

            // Создаем поток для чтения данных
            Stream fStream = new FileStream(Path, FileMode.Open, FileAccess.Read);

            // Запускаем процесс десериализации
            dlist = xmlSerializer.Deserialize(fStream) as ObservableCollection<Department>;

            // Закрываем поток
            fStream.Close();

            DepartmentList = dlist;
        }
        /// <summary>
        /// выгрузить в JSON
        /// </summary>
        /// <param name="filepath"></param>
        public void ExportDatatoJSON(string filepath)
        {
            filepath += "\\ExportDataXML.json";
            // Создаем сериализатор на основе указанного типа 
            string json = JsonConvert.SerializeObject(DepartmentList);

            File.WriteAllText(filepath, json);
        }
        /// <summary>
        /// Загрузить из JSON
        /// </summary>
        /// <param name="Path"></param>
        public void ImportDataJSON(string Path)
        {
            string json = File.ReadAllText(Path);
            DepartmentList = JsonConvert.DeserializeObject<ObservableCollection<Department>>(json);
        }
    }
}
