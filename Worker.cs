﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Homework_08
{
    /// <summary>
    ////Класс работника
    /// </summary>
    public class Worker 
    {
        /// <summary>
        /// Имя 
        /// </summary>   
        public string Firstname { get; set; }
        /// <summary>
        ///Фамилия
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        ///Возраст
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Департамент
        /// </summary>
        public string Department { get; set; }
        /// <summary>
        /// Оплата труда
        /// </summary>
        public int Pay { get; set; }
        /// <summary>
        /// Количество проектов
        /// </summary>
        public int Project { get; set; }
    }
}
