﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;


namespace Homework_08
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        InfoSystem gsystem = new InfoSystem();
        public MainWindow()
        {
            /// Создать прототип информационной системы, в которой есть возможност работать со структурой организации
            /// В структуре присутствуют департаменты и сотрудники
            /// Каждый департамент может содержать не более 1_000_000 сотрудников.
            /// У каждого департамента есть поля: наименование, дата создания,
            /// количество сотрудников числящихся в нём 
            /// (можно добавить свои пожелания)
            /// 
            /// У каждого сотрудника есть поля: Фамилия, Имя, Возраст, департамент в котором он числится, 
            /// уникальный номер, размер оплаты труда, количество закрепленным за ним.
            ///
            /// В данной информаиционной системе должна быть возможность 
            /// - импорта и экспорта всей информации в xml и json
            /// Добавление, удаление, редактирование сотрудников и департаментов
            /// 
            /// * Реализовать возможность упорядочивания сотрудников в рамках одно департамента 
            /// по нескольким полям, например возрасту и оплате труда
            /// 
            ///  №     Имя       Фамилия     Возраст     Департамент     Оплата труда    Количество проектов
            ///  1   Имя_1     Фамилия_1          23         Отдел_1            10000                      3 
            ///  2   Имя_2     Фамилия_2          21         Отдел_2            20000                      3 
            ///  3   Имя_3     Фамилия_3          22         Отдел_1            20000                      3 
            ///  4   Имя_4     Фамилия_4          24         Отдел_1            10000                      3 
            ///  5   Имя_5     Фамилия_5          22         Отдел_2            20000                      3 
            ///  6   Имя_6     Фамилия_6          22         Отдел_1            10000                      3 
            ///  7   Имя_7     Фамилия_7          23         Отдел_1            20000                      3 
            ///  8   Имя_8     Фамилия_8          23         Отдел_1            30000                      3 
            ///  9   Имя_9     Фамилия_9          21         Отдел_1            30000                      3 
            /// 10  Имя_10    Фамилия_10          21         Отдел_2            10000                      3 
            /// 
            /// 
            /// Упорядочивание по одному полю возраст
            /// 
            ///  №     Имя       Фамилия     Возраст     Департамент     Оплата труда    Количество проектов
            ///  2   Имя_2     Фамилия_2          21         Отдел_2            20000                      3 
            /// 10  Имя_10    Фамилия_10          21         Отдел_2            10000                      3 
            ///  9   Имя_9     Фамилия_9          21         Отдел_1            30000                      3 
            ///  3   Имя_3     Фамилия_3          22         Отдел_1            20000                      3 
            ///  5   Имя_5     Фамилия_5          22         Отдел_2            20000                      3 
            ///  6   Имя_6     Фамилия_6          22         Отдел_1            10000                      3 
            ///  1   Имя_1     Фамилия_1          23         Отдел_1            10000                      3 
            ///  8   Имя_8     Фамилия_8          23         Отдел_1            30000                      3 
            ///  7   Имя_7     Фамилия_7          23         Отдел_1            20000                      3 
            ///  4   Имя_4     Фамилия_4          24         Отдел_1            10000                      3 
            /// 
            ///
            /// Упорядочивание по полям возраст и оплате труда
            /// 
            ///  №     Имя       Фамилия     Возраст     Департамент     Оплата труда    Количество проектов
            /// 10  Имя_10    Фамилия_10          21         Отдел_2            10000                      3 
            ///  2   Имя_2     Фамилия_2          21         Отдел_2            20000                      3 
            ///  9   Имя_9     Фамилия_9          21         Отдел_1            30000                      3 
            ///  6   Имя_6     Фамилия_6          22         Отдел_1            10000                      3 
            ///  3   Имя_3     Фамилия_3          22         Отдел_1            20000                      3 
            ///  5   Имя_5     Фамилия_5          22         Отдел_2            20000                      3 
            ///  1   Имя_1     Фамилия_1          23         Отдел_1            10000                      3 
            ///  7   Имя_7     Фамилия_7          23         Отдел_1            20000                      3 
            ///  8   Имя_8     Фамилия_8          23         Отдел_1            30000                      3 
            ///  4   Имя_4     Фамилия_4          24         Отдел_1            10000                      3 
            /// 
            /// 
            /// Упорядочивание по полям возраст и оплате труда в рамках одного департамента
            /// 
            ///  №     Имя       Фамилия     Возраст     Департамент     Оплата труда    Количество проектов
            ///  9   Имя_9     Фамилия_9          21         Отдел_1            30000                      3 
            ///  6   Имя_6     Фамилия_6          22         Отдел_1            10000                      3 
            ///  3   Имя_3     Фамилия_3          22         Отдел_1            20000                      3 
            ///  1   Имя_1     Фамилия_1          23         Отдел_1            10000                      3 
            ///  7   Имя_7     Фамилия_7          23         Отдел_1            20000                      3 
            ///  8   Имя_8     Фамилия_8          23         Отдел_1            30000                      3 
            ///  4   Имя_4     Фамилия_4          24         Отдел_1            10000                      3 
            /// 10  Имя_10    Фамилия_10          21         Отдел_2            10000                      3 
            ///  2   Имя_2     Фамилия_2          21         Отдел_2            20000                      3 
            ///  5   Имя_5     Фамилия_5          22         Отдел_2            20000                      3 
            /// 


            InitializeComponent();
            DepartmentGrid.ItemsSource = gsystem.GetDepartments;
        }
        /// <summary>
        /// Добавить работника в отдел
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {///я видимо очень туплю но никак не могу добиться изменения количества сотридников на дата гриде если пишу воркера в лист с датагрида
             // Поэтому будет кнопка и процедура AddWorkerToDep которая пересчитает количество записей в листе
                Worker wo = new Worker();
                gsystem.AddWorkerToDep(DepartmentGrid.SelectedIndex, wo);
            }
            catch { }
        }
        /// <summary>
        /// удалить работника с отдела
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {//ну и тоже самое с удалением, я хз как настроить отображение изменения count с листа
                //будет перерасчитываться по кнопке
                Worker wo = new Worker();
                gsystem.DelWorkerToDep(DepartmentGrid.SelectedIndex, WorkerGrid.SelectedIndex);
            }
            catch { }
        }
        /// <summary>
        /// изменение выбора в гриде отделов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DepartmentGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {//достает лист воркеров из выбранного департамента
            if(gsystem.DepartmentsCount>0  && DepartmentGrid.SelectedIndex>=0 && DepartmentGrid.SelectedIndex < gsystem.DepartmentsCount)
            WorkerGrid.ItemsSource = gsystem.GetWorkerList(DepartmentGrid.SelectedIndex);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                gsystem.sortRecord(CBSort.SelectedIndex, CBSort2.SelectedIndex, DepartmentGrid.SelectedIndex);
            }
            catch { }
        }

        private void ExportXML_Click(object sender, RoutedEventArgs e)
        {
            //диалог по выбору файла
            System.Windows.Forms.FolderBrowserDialog FBD = new System.Windows.Forms.FolderBrowserDialog();
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                    gsystem.ExportDatatoXML(FBD.SelectedPath);
            }
        }

        private void ImportXML_Click(object sender, RoutedEventArgs e)
        {
            //диалог по выбору файла
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {//пишем на лист
                gsystem.ImportDataXML(dialog.FileName);
            }
            DepartmentGrid.ItemsSource = gsystem.GetDepartments;
        }

        private void ExportJSON_Click(object sender, RoutedEventArgs e)
        {
            //диалог по выбору файла
            System.Windows.Forms.FolderBrowserDialog FBD = new System.Windows.Forms.FolderBrowserDialog();
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gsystem.ExportDatatoJSON(FBD.SelectedPath);
            }
        }
        private void ImportJSON_Click(object sender, RoutedEventArgs e)
        {
            //диалог по выбору файла
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {//пишем на лист
                gsystem.ImportDataJSON(dialog.FileName);
            }
            DepartmentGrid.ItemsSource = gsystem.GetDepartments;
        }
    }
}
