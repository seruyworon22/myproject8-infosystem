﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Homework_08
{
    public class Department : INotifyPropertyChanged
    {
        /// <summary>
        /// Лист работников в департаменте
        /// </summary>
        private ObservableCollection<Worker> WorkersList = new ObservableCollection<Worker>();
        //количество записей в листе
        private int workercount;
        /// <summary>
        /// Название 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Количество сотрудников
        /// </summary>
        public int CountWorker
        {
            get { return workercount; }
            set
            {
                workercount = WorkersList.Count;
                OnPropertyChanged("CountWorker");
            }
        }
        /// <summary>
        /// Вернуть список сотрудников
        /// </summary>
        public ObservableCollection<Worker> GetWorkers
        {
            get { return WorkersList; }
            set
            {
                WorkersList = value;
            }
        }
        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="worker"></param>
        public void AddWorker(Worker worker)
        {
            if (WorkersList.Count() <= 1000000)
            {
                WorkersList.Add(worker);
                CountWorker = WorkersList.Count();
            }
            else
            {
                MessageBox.Show("В департаменте не может быть дольше 1 000 000 сотрудников");
            }
        }
        /// <summary>
        /// Удаляет работника
        /// </summary>
        /// <param name="index"></param>
        public void DelWorker(int index)
        {
            WorkersList.RemoveAt(index);
                CountWorker = WorkersList.Count();
        }
        /// <summary>
        /// Сортирует записи
        /// </summary>
        /// <param name="field"></param>
        /// <param name="field2"></param>
        public void sortRecord(int field, int field2)
        {
            //
            if (field != -1 && field2 != -1)
            {
                //я так и не придумал как избежать case, водимо без них никак
                List<Worker> sortedList = new List<Worker>();
                switch (field)
                {
                    case 0:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Firstname).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                    case 1:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Surname).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                    case 2:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Age).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                    case 3:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Department).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                    case 4:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Pay).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                    case 5:
                        switch (field2)
                        {
                            case 0:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Firstname).ToList();
                                break;
                            case 1:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Surname).ToList();
                                break;
                            case 2:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Age).ToList();
                                break;
                            case 3:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Department).ToList();
                                break;
                            case 4:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Pay).ToList();
                                break;
                            case 5:
                                sortedList = WorkersList.OrderBy(u => u.Project).ThenBy(u => u.Project).ToList();
                                break;
                        }
                        break;
                }



                for (int q = 0; q < sortedList.Count; q++)
                {
                    WorkersList[q] = sortedList[q];

                }
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }
}
